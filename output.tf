output "cluster_name" {
  value = module.eks-module.cluster_name
  sensitive = true
}

output "vpc_id" {
  value = module.vpc.vpc_id
  sensitive = true
}

output "kubeconfig" {
  value     = module.eks-module.kubeconfig
  sensitive = true
}

output "account_id" {
  value     = data.aws_caller_identity.current.account_id
  sensitive = true
}