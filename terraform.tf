# Copyright (c) HashiCorp, Inc.
# SPDX-License-Identifier: MPL-2.0

terraform {
  cloud {}

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "4.47.0"
    }

    local = {
      source  = "hashicorp/local"
      version = "1.4.0"
    }

    template = {
      source  = "hashicorp/template"
      version = "2.2.0"
    }

    kubernetes = {
      source = "hashicorp/kubernetes"
    }

    tls = {
      source  = "hashicorp/tls"
      version = "4.0.4"
    }
  }

  required_version = ">= 1.4.0"
}
