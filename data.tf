data "aws_availability_zones" "azs" {
  state = "available"
}

data "aws_eks_cluster" "cluster" {
  name = module.eks-module.id
}

data "aws_eks_cluster_auth" "cluster" {
  name = module.eks-module.id
}

data "aws_caller_identity" "current" {}
