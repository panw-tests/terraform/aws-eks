module "eks-module" {
  source                   = "app.terraform.io/jfmontufar32/eks-module/aws"
  version                  = ">= 1.0.0"
  name                     = var.cluster_name
  cluster_version          = var.cluster_version
  environment              = var.environment
  private_subnets_ids      = module.vpc.private_subnets
  public_subnets_ids       = module.vpc.public_subnets
  namespaces               = var.namespaces
  kubeconfig_template_path = var.kubeconfig_template_path
}
