module "vpc" {
  source = "terraform-aws-modules/vpc/aws"

  name = "${var.vpc_name}-${var.environment}"
  cidr = var.vpc_cidr

  azs             = slice(data.aws_availability_zones.azs.names, 0, length(var.private_subnets))
  private_subnets = var.private_subnets
  public_subnets  = var.public_subnets

  enable_nat_gateway     = var.enable_nat_gateway
  single_nat_gateway     = var.single_nat_gateway
  one_nat_gateway_per_az = var.one_nat_gateway_per_az
  enable_vpn_gateway     = var.enable_vpn_gateway

  tags = {
    Environment = var.environment
  }

  private_subnet_tags = {
    Environment                                                    = var.environment
    "kubernetes.io/cluster/${var.cluster_name}-${var.environment}" = var.loadbalancer_type
    "kubernetes.io/role/internal-elb"                              = 1
  }

  public_subnet_tags = {
    Environment                                                    = var.environment
    "kubernetes.io/cluster/${var.cluster_name}-${var.environment}" = var.loadbalancer_type
    "kubernetes.io/role/elb"                                       = 1
  }
}