variable "region" {
  type        = string
  description = "AWS region where infrastructure will be deployed"
}

variable "environment" {
  type        = string
  description = "Environment where will be deployed"
}

variable "vpc_name" {
  type        = string
  description = "Name of the VPC"
}

variable "vpc_cidr" {
  type        = string
  description = "CIDR of the VPC"
}

variable "private_subnets" {
  type        = list(any)
  description = "CIDRs of private subnets"
}

variable "public_subnets" {
  type        = list(any)
  description = "CIDRs of public subnets"
}

variable "enable_nat_gateway" {
  type        = bool
  description = "Add one Nat per Availabilty Zone"
}

variable "single_nat_gateway" {
  type        = bool
  description = "Add a single nat for the VPC"
}

variable "one_nat_gateway_per_az" {
  type        = bool
  description = "Add one Nat per Availabilty Zone"
}

variable "enable_vpn_gateway" {
  type        = bool
  default     = false
  description = "Enables VPN Gateway for the VPC"
}

variable "loadbalancer_type" {
  type        = string
  default     = "shared"
  description = "Load Balancers type created by the EKS Cluster"
}

variable "cluster_name" {
  type        = string
  description = "Name of the EKS Cluster"
}

variable "cluster_version" {
  type        = string
  default     = "1.25"
  description = "Kubernetes version of the EKS Cluster"
}

variable "namespaces" {
  type    = list(any)
  default = ["kube-system", "default", "gitlab-agent", "aws-observability", "frontend", "backend"]
}

variable "kubeconfig_template_path" {
  type    = string
  default = "kubeconfig.tftpl"
}

variable "ingress_controller_name" {
  type        = string
  default     = "alb-ingress-controller"
  description = "Ingress controller name"
}